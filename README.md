# CompressingAndroidImages
Compress images of android project with Guetzli.

- [x] compressing images that below res directory in shell script.
- [x] compressing images in github-action and then commit new file.
- [x] compressing images in gitlab-ci and then commit new file.
- [ ] supporting FLIF format.

# Remind 
If you want to clone this repo, please install [git-lfs](https://git-lfs.github.com/) before cloning.